﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace WPFDataGridDragMove
{
    class CellData
    {
        public string Data1 { get; set; }
        public string Data2 { get; set; }
    }

    /// <summary>
    /// MainWindow.xaml の相互作用ロジック
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();

            var items = new List<CellData>();
            items.Add(new CellData() { Data1 = "Data11", Data2 = "Data12" });
            items.Add(new CellData() { Data1 = "Data21", Data2 = "Data22" });
            items.Add(new CellData() { Data1 = "Data31", Data2 = "Data32" });
            dataGrid.ItemsSource = items;
        }

        private void Window_ContentRendered(object sender, EventArgs e)
        {

        }

        private void DataGrid_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            // 選択されたDataGrid1のセルの列番号、行番号を取得します。
            var dataGrid = sender as DataGrid;
            var columnIndex = dataGrid.CurrentCell.Column.DisplayIndex;
            var rowIndex = dataGrid.Items.IndexOf(dataGrid.CurrentItem);

            // 選択されたセルを取得します。
            var row = dataGrid.ItemContainerGenerator.ContainerFromIndex(rowIndex) as DataGridRow;
            var cell = dataGrid.Columns[columnIndex].GetCellContent(row);

            // cellの型は、DataGridを編集可能にした場合はTextBox
            // 編集不可にした場合はTextBlockになります。
            var data = cell as TextBlock;

            // ドラッグアンドドロップ動作を開始します。
            // 第2引数dataの型は、Dropした側で取り出すときの型と合わせないとクラッシュします。
            DragDrop.DoDragDrop(dataGrid, data, DragDropEffects.Copy);
        }

        private void DataGrid_DragEnter(object sender, DragEventArgs e)
        {
            // ドラッグアンドドロップの効果をコピーとする。
            e.Effects = DragDropEffects.Copy;
        }

        /**
         * @brief データグリッドに
        private void DataGrid_Drop(object sender, DragEventArgs e)
        {
            var dataGrid = sender as DataGrid;

            // 元データを取得する。
            var src = e.Data.GetData(typeof(TextBlock)) as TextBlock;

            var point = e.GetPosition(dataGrid);
            var dst = GetDataGridCell<DataGridCell>(dataGrid, point);
            if (dst == null) return;

            var content = dst.Content;
            if (!(content is TextBlock)) return;

            var dstTextBlock = content as TextBlock;
            dstTextBlock.Text = src.Text;
        }

        // 内部メソッド(詳細は省略)

        public T GetDataGridCell<T>(DataGrid dataGrid, Point point)
        {
            T result = default(T);
            var hitResultTest = VisualTreeHelper.HitTest(dataGrid, point);
            if (hitResultTest != null)
            {
                var visualHit = hitResultTest.VisualHit;
                while (visualHit != null)
                {
                    if (visualHit is T)
                    {
                        result = (T)(object)visualHit;
                        break;
                    }
                    visualHit = VisualTreeHelper.GetParent(visualHit);
                }
            }
            return result;
        }
    }
}
